import os
from abc import ABC
from typing import List

import numpy as np

from random import randint

import values
from cv2 import cv2 as cv
from numpy import ndarray

__all__ = ['task', 'module_runner_4']

MIN_COLOR_VALUE = 0
MAX_COLOR_VALUE = 1
COUNT_ACCESSIBLE_SHAPES = 3
WORKDIR = os.getcwd() + '/workdir/module_4/'
FILENAME = 'pict_in'
EXTENSION = '.jpg'


class AbstractMatrix(ABC):
    div: int
    values: List[List[int]]


class BaseMatrix(AbstractMatrix):
    div = 1
    values = []


class NoiseSmoothingA(BaseMatrix):
    div = 9
    values = np.array([
        [1, 1, 1],
        [1, 1, 1],
        [1, 1, 1],
    ])


class NoiseSmoothingB(BaseMatrix):
    div = 10
    values = [
        [1, 1, 1],
        [1, 2, 1],
        [1, 1, 1],
    ]


class HorAndVertLines(BaseMatrix):
    div = 16
    values = [
        [1, 2, 1],
        [2, 4, 2],
        [1, 2, 1],
    ]


class DiagonalLines(BaseMatrix):
    div = 16
    values = [
        [2, 1, 2],
        [2, 4, 2],
        [2, 1, 2],
    ]


class HighlightingBordersA(BaseMatrix):
    values = [
        [1, 1, 1],
        [1, -2, 1],
        [-1, -1, -1],
    ]


class HighlightingBordersB(BaseMatrix):
    values = [
        [-1, -1, -1],
        [-1, 9, -1],
        [-1, -1, -1],
    ]


class HighlightingBordersC(BaseMatrix):
    values = [
        [1, -2, 1],
        [-2, 5, -2],
        [1, -2, 1],
    ]


class WithoutGradientsA(BaseMatrix):
    values = [
        [0, -1, 0],
        [-1, 4, -1],
        [0, -1, 0],
    ]


class WithoutGradientsB(BaseMatrix):
    values = [
        [-1, -1, -1],
        [-1, 0, -1],
        [-1, -1, -1],
    ]


class WithoutGradientsC(BaseMatrix):
    values = [
        [1, -2, 1],
        [-2, 4, -2],
        [1, -2, 1],
    ]


class FigureManager:
    """
    Занмаеться управленем фигур для генератора изображений
    """

    def __init__(self, image: ndarray):
        self.img = image
        self.figures = []
        self.img_size_x = 0
        self.img_size_y = 0
        self._get_properties()

    def add_random_figure(self, count=1) -> None:
        """ Выбирает случайню фигуру и генерирует её """
        for i in range(count):
            random_shape = randint(1, COUNT_ACCESSIBLE_SHAPES)
            if random_shape == 1:
                self._random_line()
            elif random_shape == 2:
                self._random_rectangle()
            elif random_shape == 3:
                self._random_triangle()

    def _get_properties(self):
        """ Получает свойства объекта для дальнейших манипуляций с изображенияем """
        p = self.img.shape
        self.img_size_x = p[0]
        self.img_size_y = p[1]

    def _randomize_color(self) -> tuple:
        """ Гениерирует однотонный цвет, который будет у фигуры """
        r: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)
        g: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)
        b: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)

        while r == 0 and g == 0 and b == 0:
            r: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)
            g: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)
            b: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)

        return b, g, r

    def _random_rectangle(self):
        """ Гениерирует случаный квадрат случсайного цвета """
        cv.rectangle(self.img, self._random_point(), self._random_point(), self._randomize_color(), thickness=-1)

    def _random_line(self):
        """ Гениерирует случайную окружность случайного цввета """
        cv.line(self.img, self._random_point(), self._random_point(), self._randomize_color())

    def _random_triangle(self) -> None:
        """ Гениерирует случайную окружность случайного цвета """
        first_point = prev_point = self._random_point()
        count_of_angles = 3
        last_angle = count_of_angles - 1
        color = self._randomize_color()

        for i in range(count_of_angles):
            if i != last_angle:
                new_point = self._random_point()
                cv.line(self.img, prev_point, new_point, color)
                prev_point = new_point
            else:
                cv.line(self.img, prev_point, first_point, color)

    def _random_point(self) -> tuple:
        """ Получает случайную точку для фигуры с учетом размеров изображения """
        x = randint(0, self.img_size_x)
        y = randint(0, self.img_size_y)

        return x, y


def _generate_img():
    """ Generate new image for module """
    empty_img: ndarray = np.zeros((500, 500, 3))
    fig_manager = FigureManager(empty_img)
    fig_manager.add_random_figure(count=5)

    # Конвертирует изображение в формат из num py open cv
    empty_img = cv.convertScaleAbs(empty_img, alpha=255.0)
    cv.imshow(WORKDIR + FILENAME + EXTENSION, empty_img.astype(np.uint8))
    # cv.waitKey(0)
    # cv.destroyAllWindows()
    cv.imwrite(WORKDIR + FILENAME + EXTENSION, empty_img.astype(np.uint8))


def task(matrix) -> None:
    """ перевод изображения в негатив """
    img = cv.imread(WORKDIR + FILENAME + EXTENSION)
    window_start_limit = (0, 0)
    window_end_limit = img.shape[0], img.shape[1]
    print('Ожидание обработки...')
    # Проходимся по цветам
    for i in range(len(img.shape)):
        # Проходимся по каждому пикселю в частности
        for y_idx, y_px in enumerate(img[:][:][i]):
            for x_idx, x_px in enumerate(img[y_idx, :, i]):
                # Пропускаем пиксели, если мы не можем взять окно 3х3 для обработки
                if ((y_idx in window_start_limit or y_idx in window_end_limit) or
                        x_idx in window_start_limit or x_idx in window_end_limit):
                    continue
                # Берем элементы для окна
                left_up_win_segment = img[y_px - 1, x_px - 1, i]
                up_win_segment = img[y_px, x_px - 1, i]
                right_up_win_segment = img[y_px + 1, x_px - 1, i]

                left_win_segment = img[y_px - 1, x_px, i]
                central_win_segment = img[y_px, x_px, i]
                right_win_segment = img[y_px + 1, x_px, i]

                left_down_win_segment = img[y_px - 1, x_px + 1, i]
                down_win_segment = img[y_px, x_px + 1, i]
                right_down_win_segment = img[y_px + 1, x_px + 1, i]

                # Закидываем в массив для удобства
                win_pixels = np.array([
                    [left_up_win_segment, up_win_segment, right_up_win_segment],
                    [left_win_segment, central_win_segment, right_win_segment],
                    [left_down_win_segment, down_win_segment, right_down_win_segment]
                ])

                # Производим математику
                win_pixels = (win_pixels.dot(matrix.values)) / matrix.div

                # присваиваем пиксели обратно в изображение
                img[y_px - 1, x_px - 1, i] = win_pixels[0, 0]
                img[y_px, x_px - 1, i] = win_pixels[0, 1]
                img[y_px + 1, x_px - 1, i] = win_pixels[0, 2]

                img[y_px - 1, x_px, i] = win_pixels[1, 0]
                img[y_px, x_px, i] = win_pixels[1, 1]
                img[y_px + 1, x_px, i] = win_pixels[1, 2]

                img[y_px - 1, x_px + 1, i] = win_pixels[2, 0]
                img[y_px, x_px + 1, i] = win_pixels[2, 1]
                img[y_px + 1, x_px + 1, i] = win_pixels[2, 2]

    # cv.filter2D()
    print('Обработка завершена!')
    cv.imshow('Negative', img)
    cv.waitKey(0)
    cv.destroyAllWindows()


def module_runner_4():
    """ Using for run module """
    _generate_img()
    matrix = NoiseSmoothingB()
    task(matrix)
