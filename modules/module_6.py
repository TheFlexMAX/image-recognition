import os

import numpy as np

from random import randint

from cv2 import cv2 as cv
from numpy import ndarray
from matplotlib import pyplot as plt

__all__ = ['task_1', 'task_2', 'task_3', 'module_runner_6']

MIN_COLOR_VALUE = 0
MAX_COLOR_VALUE = 1
COUNT_ACCESSIBLE_SHAPES = 3
WORKDIR = os.getcwd() + '/workdir/module_5/'
FILENAME = 'pict_in'
EXTENSION = '.jpg'


class FigureManager:
    """
    Занмаеться управленем фигур для генератора изображений
    """

    def __init__(self, image: ndarray):
        self.img = image
        self.figures = []
        self.img_size_x = 0
        self.img_size_y = 0
        self._get_properties()

    def add_random_figure(self, count=1) -> None:
        """ Выбирает случайню фигуру и генерирует её """
        for i in range(count):
            random_shape = randint(1, COUNT_ACCESSIBLE_SHAPES)
            if random_shape == 1:
                self._random_line()
            elif random_shape == 2:
                self._random_rectangle()
            elif random_shape == 3:
                self._random_triangle()

    def _get_properties(self):
        """ Получает свойства объекта для дальнейших манипуляций с изображенияем """
        p = self.img.shape
        self.img_size_x = p[0]
        self.img_size_y = p[1]

    def _randomize_color(self) -> tuple:
        """ Гениерирует однотонный цвет, который будет у фигуры """
        r: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)
        g: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)
        b: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)

        while r == 0 and g == 0 and b == 0:
            r: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)
            g: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)
            b: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)

        return b, g, r

    def _random_rectangle(self):
        """ Гениерирует случаный квадрат случсайного цвета """
        cv.rectangle(self.img, self._random_point(), self._random_point(), self._randomize_color(), thickness=-1)

    def _random_line(self):
        """ Гениерирует случайную окружность случайного цввета """
        cv.line(self.img, self._random_point(), self._random_point(), self._randomize_color())

    def _random_triangle(self) -> None:
        """ Гениерирует случайную окружность случайного цвета """
        first_point = prev_point = self._random_point()
        count_of_angles = 3
        last_angle = count_of_angles - 1
        color = self._randomize_color()

        for i in range(count_of_angles):
            if i != last_angle:
                new_point = self._random_point()
                cv.line(self.img, prev_point, new_point, color)
                prev_point = new_point
            else:
                cv.line(self.img, prev_point, first_point, color)

    def _random_point(self) -> tuple:
        """ Получает случайную точку для фигуры с учетом размеров изображения """
        x = randint(0, self.img_size_x)
        y = randint(0, self.img_size_y)

        return x, y


def _generate_img():
    """ Generate new image for module """
    empty_img: ndarray = np.zeros((500, 500, 3))
    fig_manager = FigureManager(empty_img)
    fig_manager.add_random_figure(count=5)

    # Конвертирует изображение в формат из num py open cv
    empty_img = cv.convertScaleAbs(empty_img, alpha=255.0)
    cv.imshow(WORKDIR + FILENAME + EXTENSION, empty_img.astype(np.uint8))
    # cv.waitKey(0)
    # cv.destroyAllWindows()
    cv.imwrite(WORKDIR + FILENAME + EXTENSION, empty_img.astype(np.uint8))


def _get_window_median(window) -> int:
    win_pixels_flatten = np.asarray(window).reshape(-1)
    win_pixels_flatten.sort()
    median_idx = len(win_pixels_flatten) // 2

    return win_pixels_flatten[median_idx]


def task_1() -> None:
    """ перевод изображения в негатив """
    img = cv.imread(WORKDIR + FILENAME + EXTENSION)
    img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

    width, height = img.shape[0], img.shape[1]
    center = [width // 2, height // 2]
    x, y = np.ogrid[:, :]
    r = 80

    mask = np.ones((width, height, 2), np.uint8)
    mask_area = ((x - center[0]) ** 2 + (y - center[1]) ** 2) <= r ^ 2
    mask[mask_area] = 0

    dft_shift = np.fft.fftshift(cv.dft(np.float32(img), flags=cv.DFT_COMPLEX_OUTPUT))

    img_back = cv.idft(np.fft.ifftshift(dft_shift * mask))
    img_back = cv.magnitude(img_back[:, :, 0], img_back[:, :, 1])

    fig = plt.figure(figsize=(12, 12))
    edited_img = fig.add_subplot(2, 2, 1)
    edited_img.imshow(img_back, cmap='gray')

    plt.savefig('task_1.png', bbox_inches="tight")


def task_2() -> None:
    img = cv.imread(WORKDIR + FILENAME + EXTENSION)
    img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

    r = 30

    ham = np.hamming(img.shape[0])[:, None]
    ham1 = np.hamming(img.shape[1])[:, None]
    ham2d = np.sqrt(np.dot(ham, ham1.T)) ** r

    f = cv.dft(img.astype(np.float32), flags=cv.DFT_COMPLEX_OUTPUT)
    f_shifted = np.fft.fftshift(f)
    f_complex = f_shifted[:, :, 0] * 1j + f_shifted[:, :, 1]
    f_filtered = f_complex * ham2d
    f_filtered_shifted = np.fft.fftshift(f_filtered)

    inv_img = np.fft.ifft2(f_filtered_shifted)

    filtered_img = np.abs(inv_img)
    filtered_img = filtered_img.astype(np.uint8)

    cv.imwrite('task_2.png', np.real(filtered_img))


def task_3():
    img = cv.imread(WORKDIR + FILENAME + EXTENSION)
    img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

    n = 10
    (width, height) = img.shape

    F1 = np.fft.fft2(img.astype(float))
    F2 = np.fft.fftshift(F1)

    half_w, half_h = width // 2, height // 2
    F2[half_w - n:half_w + n + 1, half_h - n:half_h + n + 1] = 0

    img1 = np.fft.ifft2(np.fft.ifftshift(F2)).real

    cv.imwrite('task_3.png', img1)


def module_runner_6():
    """ Using for run module """
    # _generate_img()
    # task_1()
    # task_2()
    task_3()
