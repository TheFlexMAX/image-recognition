import os
from abc import ABC
from math import sqrt
from typing import List

import numpy as np

from random import randint

import values
from cv2 import cv2 as cv
from numpy import ndarray

__all__ = ['task_1', 'module_runner_5']

MIN_COLOR_VALUE = 0
MAX_COLOR_VALUE = 1
COUNT_ACCESSIBLE_SHAPES = 3
WORKDIR = os.getcwd() + '/workdir/module_5/'
FILENAME = 'pict_in'
EXTENSION = '.jpg'


class FigureManager:
    """
    Занмаеться управленем фигур для генератора изображений
    """

    def __init__(self, image: ndarray):
        self.img = image
        self.figures = []
        self.img_size_x = 0
        self.img_size_y = 0
        self._get_properties()

    def add_random_figure(self, count=1) -> None:
        """ Выбирает случайню фигуру и генерирует её """
        for i in range(count):
            random_shape = randint(1, COUNT_ACCESSIBLE_SHAPES)
            if random_shape == 1:
                self._random_line()
            elif random_shape == 2:
                self._random_rectangle()
            elif random_shape == 3:
                self._random_triangle()

    def _get_properties(self):
        """ Получает свойства объекта для дальнейших манипуляций с изображенияем """
        p = self.img.shape
        self.img_size_x = p[0]
        self.img_size_y = p[1]

    def _randomize_color(self) -> tuple:
        """ Гениерирует однотонный цвет, который будет у фигуры """
        r: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)
        g: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)
        b: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)

        while r == 0 and g == 0 and b == 0:
            r: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)
            g: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)
            b: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)

        return b, g, r

    def _random_rectangle(self):
        """ Гениерирует случаный квадрат случсайного цвета """
        cv.rectangle(self.img, self._random_point(), self._random_point(), self._randomize_color(), thickness=-1)

    def _random_line(self):
        """ Гениерирует случайную окружность случайного цввета """
        cv.line(self.img, self._random_point(), self._random_point(), self._randomize_color())

    def _random_triangle(self) -> None:
        """ Гениерирует случайную окружность случайного цвета """
        first_point = prev_point = self._random_point()
        count_of_angles = 3
        last_angle = count_of_angles - 1
        color = self._randomize_color()

        for i in range(count_of_angles):
            if i != last_angle:
                new_point = self._random_point()
                cv.line(self.img, prev_point, new_point, color)
                prev_point = new_point
            else:
                cv.line(self.img, prev_point, first_point, color)

    def _random_point(self) -> tuple:
        """ Получает случайную точку для фигуры с учетом размеров изображения """
        x = randint(0, self.img_size_x)
        y = randint(0, self.img_size_y)

        return x, y


def _generate_img():
    """ Generate new image for module """
    empty_img: ndarray = np.zeros((500, 500, 3))
    fig_manager = FigureManager(empty_img)
    fig_manager.add_random_figure(count=5)

    # Конвертирует изображение в формат из num py open cv
    empty_img = cv.convertScaleAbs(empty_img, alpha=255.0)
    cv.imshow(WORKDIR + FILENAME + EXTENSION, empty_img.astype(np.uint8))
    # cv.waitKey(0)
    # cv.destroyAllWindows()
    cv.imwrite(WORKDIR + FILENAME + EXTENSION, empty_img.astype(np.uint8))


def _get_window_median(window) -> int:
    win_pixels_flatten = np.asarray(window).reshape(-1)
    win_pixels_flatten.sort()
    median_idx = len(win_pixels_flatten) // 2

    return win_pixels_flatten[median_idx]


def task_1() -> None:
    """ перевод изображения в негатив """
    img = cv.imread(WORKDIR + FILENAME + EXTENSION)

    window_start_limit = (0, 0)
    window_end_limit = img.shape[0] - 1, img.shape[1] - 1
    img_height = img.shape[0]
    img_width = img.shape[1]

    # Проходимся по цветам
    for i in range(img.shape[2]):
        # Проходимся по каждому пикселю в частности
        for y_idx in range(img_height):
            for x_idx in range(img_width):
                # Пропускаем пиксели, если мы не можем взять окно 3х3 для обработки
                if ((y_idx in window_start_limit or y_idx in window_end_limit) or
                        x_idx in window_start_limit or x_idx in window_end_limit):
                    continue
                # Берем элементы для окна
                left_up_win_segment = img[y_idx - 1, x_idx - 1, i]
                up_win_segment = img[y_idx, x_idx - 1, i]
                right_up_win_segment = img[y_idx + 1, x_idx - 1, i]

                left_win_segment = img[y_idx - 1, x_idx, i]
                central_win_segment = img[y_idx, x_idx, i]
                right_win_segment = img[y_idx + 1, x_idx, i]

                left_down_win_segment = img[y_idx - 1, x_idx + 1, i]
                down_win_segment = img[y_idx, x_idx + 1, i]
                right_down_win_segment = img[y_idx + 1, x_idx + 1, i]

                # Закидываем в массив для удобства
                win_pixels = np.array([
                    [left_up_win_segment, up_win_segment, right_up_win_segment],
                    [left_win_segment, central_win_segment, right_win_segment],
                    [left_down_win_segment, down_win_segment, right_down_win_segment]
                ])

                # высчитываю среднее для выбранного окна
                img[y_idx, x_idx, i] = _get_window_median(win_pixels)

    cv.imshow('Negative', img)
    cv.waitKey(0)
    cv.destroyAllWindows()


def task_2() -> None:
    img = cv.imread(WORKDIR + FILENAME + EXTENSION)
    print("\nSobel \n1 - sqrt(x*x+y*y) \n2 - abs(x)+abs(y) \nRoberts \n3 - sqrt(u*u+v*v) \n4 - abs(u)+abs(v)")
    method = int(input("method: "))
    for i in range(0, img.shape[0]):
        for j in range(0, img.shape[1]):
            jj = j
            ii = i
            if jj == img.shape[1] - 1: jj = -1
            if ii == img.shape[0] - 1: ii = -1
            colorij = [img[i - 1, j - 1], img[i - 1, j], img[i - 1, jj + 1], img[i, j - 1], img[i, j],
                       img[i, jj + 1], img[ii + 1, j - 1], img[ii + 1, j], img[ii + 1, jj + 1]]
            if method == 1 or method == 2:
                rx = colorij[2][0] + 2 * colorij[5][0] + colorij[8][0] - colorij[0][0] - 2 * colorij[3][0] - colorij[6][
                    0]
                ry = colorij[0][0] + 2 * colorij[1][0] + colorij[2][0] - colorij[6][0] - 2 * colorij[7][0] - colorij[8][
                    0]
                if method == 1: r = sqrt(rx ** 2 + ry ** 2)
                if method == 2: r = abs(rx) + abs(ry)
                gx = colorij[2][1] + 2 * colorij[5][1] + colorij[8][1] - colorij[0][1] - 2 * colorij[3][1] - colorij[6][
                    1]
                gy = colorij[0][1] + 2 * colorij[1][1] + colorij[2][1] - colorij[6][1] - 2 * colorij[7][1] - colorij[8][
                    2]
                if method == 1: g = sqrt(gx ** 2 + gy ** 2)
                if method == 2: g = abs(gx) + abs(gy)
                bx = colorij[2][2] + 2 * colorij[5][2] + colorij[8][2] - colorij[0][2] - 2 * colorij[3][2] - colorij[6][
                    2]
                by = colorij[0][2] + 2 * colorij[1][2] + colorij[2][2] - colorij[6][2] - 2 * colorij[7][2] - colorij[8][
                    2]
                if method == 1:
                    b = sqrt(bx ** 2 + by ** 2)
                if method == 2:
                    b = abs(bx) + abs(by)
            if method == 3 or method == 4:
                rx = int(colorij[8][0] - colorij[4][0])
                ry = int(colorij[7][0] - colorij[5][0])
                if method == 3:
                    r = sqrt(rx ** 2 + ry ** 2)
                if method == 4:
                    r = abs(rx) + abs(ry)
                gx = int(colorij[8][1] - colorij[4][1])
                gy = int(colorij[7][1] - colorij[5][1])
                if method == 3:
                    g = sqrt(gx ** 2 + gy ** 2)
                if method == 4:
                    g = abs(gx) + abs(gy)
                bx = int(colorij[8][2] - colorij[4][2])
                by = int(colorij[7][2] - colorij[5][2])
                if method == 3:
                    b = sqrt(bx ** 2 + by ** 2)
                if method == 4:
                    b = abs(bx) + abs(by)
                r += colorij[4][0]
                g += colorij[4][1]
                b += colorij[4][2]

            while r < 0 or r > 255:
                if r < 0:
                    r = 0
                if r > 255:
                    r = 255
            while g < 0 or g > 255:
                if g < 0:
                    g = 0
                if g > 255:
                    g = 255
            while b < 0 or b > 255:
                if b < 0:
                    b = 0
                if b > 255:
                    b = 255
            img[i, j] = (r, g, b)

    cv.imshow('Negative', img)
    cv.waitKey(0)
    cv.destroyAllWindows()


def module_runner_5():
    """ Using for run module """
    # _generate_img()
    # task_1()
    task_2()
