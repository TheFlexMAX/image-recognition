import os
import numpy as np
import matplotlib.pyplot as plt

from random import randint

from cv2 import cv2 as cv
from numpy import ndarray

__all__ = ['task_1', 'task_2', 'task_3', 'task_4', 'task_5', 'task_6', 'task_7', 'module_runner_1']

MIN_COLOR_VALUE = 0
MAX_COLOR_VALUE = 1
COUNT_ACCESSIBLE_SHAPES = 3
WORKDIR = os.getcwd() + '/workdir/module_1/'
FILENAME = 'random_art'


class FigureManager:
    """
    Занмаеться управленем фигур для генератора изображений
    """

    def __init__(self, image: ndarray):
        self.img = image
        self.figures = []
        self.img_size_x = 0
        self.img_size_y = 0
        self._get_properties()

    def add_random_figure(self, count=1) -> None:
        """ Выбирает случайню фигуру и генерирует её """
        for i in range(count):
            random_shape = randint(1, COUNT_ACCESSIBLE_SHAPES)
            if random_shape == 1:
                self._random_line()
            elif random_shape == 2:
                self._random_rectangle()
            elif random_shape == 3:
                self._random_triangle()

    def _get_properties(self):
        """ Получает свойства объекта для дальнейших манипуляций с изображенияем """
        p = self.img.shape
        self.img_size_x = p[0]
        self.img_size_y = p[1]

    def _randomize_color(self) -> tuple:
        """ Гениерирует однотонный цвет, который будет у фигуры """
        r: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)
        g: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)
        b: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)

        while r == 0 and g == 0 and b == 0:
            r: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)
            g: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)
            b: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)

        return b, g, r

    def _random_rectangle(self):
        """ Гениерирует случаный квадрат случсайного цвета """
        cv.rectangle(self.img, self._random_point(), self._random_point(), self._randomize_color(), thickness=-1)

    def _random_line(self):
        """ Гениерирует случайную окружность случайного цввета """
        cv.line(self.img, self._random_point(), self._random_point(), self._randomize_color())

    def _random_triangle(self) -> None:
        """ Гениерирует случайную окружность случайного цвета """
        first_point = prev_point = self._random_point()
        count_of_angles = 3
        last_angle = count_of_angles - 1
        color = self._randomize_color()

        for i in range(count_of_angles):
            if i != last_angle:
                new_point = self._random_point()
                cv.line(self.img, prev_point, new_point, color)
                prev_point = new_point
            else:
                cv.line(self.img, prev_point, first_point, color)

    def _random_point(self) -> tuple:
        """ Получает случайную точку для фигуры с учетом размеров изображения """
        x = randint(0, self.img_size_x)
        y = randint(0, self.img_size_y)

        return x, y


class GreyMachine:
    """ Берет изображение и конвертирует его в то же изображение серого цвета """

    def __init__(self, image: ndarray):
        self.image = image

    def to_grey(self, custom=False):
        """ Предоставляет интерфейс для конвертации изображения в оттенки серого """
        if custom:
            self._to_custom_greyscale()
        else:
            grey = cv.cvtColor(self.image, cv.COLOR_BGR2GRAY)
            self.image = grey.astype(np.uint8)

    def _to_custom_greyscale(self):
        """ Конвертирует изображение использую numpy в изображение оттенков серого """
        blue_channel = (self.image[:, :, 0] + self.image[:, :, 1] + self.image[:, :, 2]) / 3
        green_channel = (self.image[:, :, 0] + self.image[:, :, 1] + self.image[:, :, 2]) / 3
        red_channel = (self.image[:, :, 0] + self.image[:, :, 1] + self.image[:, :, 2]) / 3
        grey_img = blue_channel + green_channel + red_channel
        self.image = grey_img.astype(np.uint8)


def task_1():
    # генерируем пустое изображение размером 500 на 500 и с 3 каналами цвета
    for i in range(10):
        empty_img: ndarray = np.zeros((500, 500, 3))
        fig_manager = FigureManager(empty_img)
        fig_manager.add_random_figure(count=5)

        # cv.imshow('Random figures', empty_img)
        # Конвертирует изображение в формат из num py open cv
        empty_img = cv.convertScaleAbs(empty_img, alpha=255.0)
        cv.imwrite(WORKDIR + FILENAME + f'_{i}' + '.jpg', empty_img.astype(np.uint8))
    # ожидание закрытия окна
    # cv.waitKey(0)
    # cv.destroyAllWindows()


def task_2():
    img = cv.imread(WORKDIR + FILENAME + '_1' + '.jpg')
    gm = GreyMachine(img)
    gm.to_grey()

    cv.imshow(WORKDIR + FILENAME + '.jpg', gm.image.astype(np.uint8))
    cv.waitKey(0)
    cv.destroyAllWindows()


def task_3():
    """ Трансформирует изображение """
    img = cv.imread(WORKDIR + FILENAME + '_2' + '.jpg')
    img = cv.flip(img, 1)
    cv.imwrite(WORKDIR + FILENAME + '_2' + '.jpg', img.astype(np.uint8))


def task_4():
    """ Поворачивает изображение на 90 градусов """
    img = cv.imread(WORKDIR + FILENAME + '_3' + '.jpg')
    img = cv.rotate(img, cv.ROTATE_90_CLOCKWISE)
    cv.imwrite(WORKDIR + FILENAME + '_3' + '.jpg', img.astype(np.uint8))


def task_5():
    """ Занимаеться масштабированием изображения в 2 раза меньше """
    img = cv.imread(WORKDIR + FILENAME + '_4' + '.jpg')
    p = img.shape
    img_size_x = p[0]
    img_size_y = p[1]

    new_img_size_x = int(img_size_x / 2)
    new_img_size_y = int(img_size_y / 2)

    img = cv.resize(img, (new_img_size_x, new_img_size_y), interpolation=cv.INTER_CUBIC)
    cv.imwrite(WORKDIR + FILENAME + '_4_new' + '.jpg', img.astype(np.uint8))


def task_6():
    """ Занимается поворачиванием изображения на 30 градусов """
    img = cv.imread(WORKDIR + FILENAME + '_5' + '.jpg')
    angle = 30

    shape = img.shape
    width = shape[0]
    height = shape[1]

    # Точка вокруг которого будет производиться вращение
    center = (width//2, height//2)
    # задает параметры того откуда поворачивать, на какой угол и в каком масштабе
    rotation_matrix = cv.getRotationMatrix2D(center, angle, 1.0)
    # Преобразует изображение с сохронением растояния между точками
    img = cv.warpAffine(img, rotation_matrix, (width, height))

    cv.imwrite(WORKDIR + FILENAME + '_5' + '.jpg', img.astype(np.uint8))


def task_7():
    """ рисует декартовую систему координат и график по заданной функции """
    """ Для этого задания используется библиотека matplotlib """
    # промежуток, на котором будет находиться
    a, b = -10, 10

    # Генерируем набор значений, по умолчанию 50
    x = np.linspace(a, b)

    # функция графика y = f(x)
    y = x

    # конфигурируем отображение системы координат
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.spines['left'].set_position('center')
    ax.spines['bottom'].set_position('center')
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')

    # передаем показания на отрисовку
    plt.plot(x, y)
    plt.draw()

    # сохраянем изображение
    plt.savefig(WORKDIR + FILENAME + '_6' + '.jpg')
    plt.close(fig)


def module_runner_1():
    """ Using for run module """
    task_1()
    task_2()
    task_3()
    task_4()
    task_5()
    task_6()
    task_7()
