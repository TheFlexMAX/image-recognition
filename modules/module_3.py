import os
import numpy as np
import matplotlib.pyplot as plt

from random import randint

from cv2 import cv2 as cv
from numpy import ndarray

__all__ = ['task', 'module_runner_3']

MIN_COLOR_VALUE = 0
MAX_COLOR_VALUE = 1
COUNT_ACCESSIBLE_SHAPES = 3
WORKDIR = os.getcwd() + '/workdir/module_3/'
FILENAME = 'pict_in'
EXTENSION = '.jpg'


class FigureManager:
    """
    Занмаеться управленем фигур для генератора изображений
    """

    def __init__(self, image: ndarray):
        self.img = image
        self.figures = []
        self.img_size_x = 0
        self.img_size_y = 0
        self._get_properties()

    def add_random_figure(self, count=1) -> None:
        """ Выбирает случайню фигуру и генерирует её """
        for i in range(count):
            random_shape = randint(1, COUNT_ACCESSIBLE_SHAPES)
            if random_shape == 1:
                self._random_line()
            elif random_shape == 2:
                self._random_rectangle()
            elif random_shape == 3:
                self._random_triangle()

    def _get_properties(self):
        """ Получает свойства объекта для дальнейших манипуляций с изображенияем """
        p = self.img.shape
        self.img_size_x = p[0]
        self.img_size_y = p[1]

    def _randomize_color(self) -> tuple:
        """ Гениерирует однотонный цвет, который будет у фигуры """
        r: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)
        g: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)
        b: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)

        while r == 0 and g == 0 and b == 0:
            r: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)
            g: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)
            b: int = randint(MIN_COLOR_VALUE, MAX_COLOR_VALUE)

        return b, g, r

    def _random_rectangle(self):
        """ Гениерирует случаный квадрат случсайного цвета """
        cv.rectangle(self.img, self._random_point(), self._random_point(), self._randomize_color(), thickness=-1)

    def _random_line(self):
        """ Гениерирует случайную окружность случайного цввета """
        cv.line(self.img, self._random_point(), self._random_point(), self._randomize_color())

    def _random_triangle(self) -> None:
        """ Гениерирует случайную окружность случайного цвета """
        first_point = prev_point = self._random_point()
        count_of_angles = 3
        last_angle = count_of_angles - 1
        color = self._randomize_color()

        for i in range(count_of_angles):
            if i != last_angle:
                new_point = self._random_point()
                cv.line(self.img, prev_point, new_point, color)
                prev_point = new_point
            else:
                cv.line(self.img, prev_point, first_point, color)

    def _random_point(self) -> tuple:
        """ Получает случайную точку для фигуры с учетом размеров изображения """
        x = randint(0, self.img_size_x)
        y = randint(0, self.img_size_y)

        return x, y


def _generate_img():
    """ Generate new image for module """
    empty_img: ndarray = np.zeros((500, 500, 3))
    fig_manager = FigureManager(empty_img)
    fig_manager.add_random_figure(count=5)

    # Конвертирует изображение в формат из num py open cv
    empty_img = cv.convertScaleAbs(empty_img, alpha=255.0)
    cv.imshow(WORKDIR + FILENAME + EXTENSION, empty_img.astype(np.uint8))
    # cv.waitKey(0)
    # cv.destroyAllWindows()
    cv.imwrite(WORKDIR + FILENAME + EXTENSION, empty_img.astype(np.uint8))


def task():
    """ перевод изображения в негатив """
    img = cv.imread(WORKDIR + FILENAME + EXTENSION)
    n = img.shape[0] * img.shape[1] * img.shape[2]
    for i in range(0, img.shape[0]):
        for j in range(0, img.shape[1]):
            color = img[i, j]
            r = 0
            for jj in range(0, color[0]):
                nr = np.count_nonzero(img == jj)
                r = r + nr / n
            r *= 255

            g = 0
            for jj in range(0, color[1]):
                ng = np.count_nonzero(img == jj)
                g = g + ng / n
            g *= 255

            b = 0
            for jj in range(0, color[2]):
                nb = np.count_nonzero(img == jj)
                b = b + nb / n
            b *= 255
            img[i, j] = (r, g, b)

    cv.imshow('Negative', img)
    cv.waitKey(0)
    cv.destroyAllWindows()


def module_runner_3():
    """ Using for run module """
    _generate_img()
    task()
