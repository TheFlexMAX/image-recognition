from modules.module_1 import module_runner_1
from modules.module_2 import module_runner_2
from modules.module_3 import module_runner_3
from modules.module_4 import module_runner_4
from modules.module_5 import module_runner_5
from modules.module_6 import module_runner_6


def modules():
    """ Contain calls for all modules of tasks """
    module_runner_1()
    # module_runner_2()
    # module_runner_3()
    # module_runner_4()
    # module_runner_5()
    # module_runner_6()


def run():
    modules()


if __name__ == '__main__':
    run()
